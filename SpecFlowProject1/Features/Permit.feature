﻿Feature: Permit application
Get an Unregistered Vehicle Permit


@mytag
Scenario: Apply for a permit
	Given a permit is required
	And the vehicle type Prime Mover
	And the address is Unit 7 11 Sample Street, Broadmeadows VIC 3047
	And the permit duration date is 1 day
	When the user clicks next
	Then the second page is shown