﻿using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using OpenQA.Selenium.Chrome;
using System.Diagnostics;
using System.Text;

namespace SpecFlowProject1.Steps
{
    [Binding]
    public sealed class PermitStepDefinitions
    {


        [Given(@"a permit is required")]
        public void GivenAPermitIsRequired()
        {
            ScenarioContext.Current.Pending();
        }


        //public class Driver
        //{
        //    public static IWebDriver Instance { get; set; }
        //    public static string Url { get; internal set; }

        //    public static void Initialize()
        //    {
        //        Instance = new ChromeDriver(@"C:\Users\chris\source\repos\VicRoads2\SpecFlowProject1\Drivers");
        //        //Instance.Manage().Window.Maximize();
        //    }

        //    public static void Close()
        //    {
        //        Instance.Close();
        //    }
        //}





        // For additional details on SpecFlow step definitions see https://go.specflow.org/doc-stepdef

        private readonly ScenarioContext _scenarioContext;

        public PermitStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }


        [Given("a permit is required")]
        public void GivenAPermitIsRequired()
        {
            Instance.Url = "https://www.vicroads.vic.gov.au/registration/limited-use-permits/unregistered-vehicle-permits/get-an-unregistered-vehicle-permit";
            //ScenarioContext.Current["Result"] = firstNumber + secondNumber;
            Assert.AreEqual("Expected title", "Actual title (var)");
        }


        [Given("the vehicle type(.*)")]
        public void ThenTheResultShouldBe(string result)
        {

            //Insert in to:
            //#ph_pagebody_0_phthreecolumnmaincontent_0_panel_VehicleType_DDList

            //TODO: implement assert (verification) logic

            _scenarioContext.Pending();
        }




        [When("the user clicks next")]
        public void WhenTheUserClicksNext()
        {
            //Click next:
            //#ph_pagebody_0_phthreecolumnmaincontent_0_panel_btnNext

            //TODO: implement assert (verification) logic

            _scenarioContext.Pending();
        }


        [Then("the second page is shown")]
        public void ThenTheResultShouldBe(int result)
        {

            Assert.AreEqual("Expected title", "Actual title (var)");
            //TODO: implement assert (verification) logic

            _scenarioContext.Pending();
        }




//Select
//#ph_pagebody_0_phthreecolumnmaincontent_0_panel_PassengerVehicleSubType_DDList


//Input address(Unit 7 11 Sample Street, Broadmeadows VIC 3047) :
//#ph_pagebody_0_phthreecolumnmaincontent_0_panel_AddressLine_SingleLine_CtrlHolderDivShown


//Permit start date
//-Use default


//Select 1 day:
//#ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitDuration_DDList

//Check for:
//#main > div > p


















    }
}
