# README #
This document describes the steps undertaken as part of a skills assessment for a job application.

---

Note: SpecFlow+ Excel is only available for SpecFlow 2.4 and earlier.
https://docs.specflow.org/projects/specflow-excel/en/latest/?_gl=1*siugf8*_ga*NDQzNDY3NzA0LjE2MzE3MDI4MzI.*_ga_BZ55XKTXC6*MTYzMTcxMDUxNy4yLjEuMTYzMTcxMTg5MS4w

---

### Prerequisites  ###
* Windows VM (hosted in Azure)
* Chrome
* Visual studio community/vs code
* NuGet
* Install Selenium webdriver (via NuGet)
* Download ChromeDriver
* Update ChromeDriver path




